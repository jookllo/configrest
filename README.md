 # KarestApp
## Introduction
The `RestConfig` API is a Spring Boot application that provides a set of endpoints for fetching values for a datacenter.
These values can be retrieved in JSON format and allow users to list all the properties of the datacenter.
This API is designed to be easy to use and flexible, making it a powerful tool for managing and accessing datacenter information.
Whether you're a developer looking to integrate the RestConfig API into your application or a user looking to access and manage your datacenter's properties,
the RestConfig API has you covered.
## Prerequisites
In order to use the RestConfig API, you will need the following:

    Java 8 or later
    MySQL database
    Postman (optional, for testing the API)

Note that the RestConfig API is currently configured to use MySQL on port 3308.
If you want to change this to the default MySQL port (3306), you can do so by modifying the `application.properties` file.
Make sure that you have all of these prerequisites installed and set up before proceeding with the installation of the RestConfig API.
## Installation 
To install the RestConfig API, follow these steps:

1. Clone the repository from [RestConfig API repository](https://thanos.cellulant.africa/tingg-digital-banking/ecobank/wallet-4.0/RestConfig/-/tree/test)
2. Open the project in your preferred Java IDE, such as Eclipse, IntelliJ, or Visual Studio Code with the Java Maven plugin.
3. Make sure MySQL (not MariaDB) is running on XAMPP or as a standalone service.
4. Run the pom.xml file to install all the dependencies for the application.
5. Run the application using your IDE or by using the `./mvnw spring-boot:run` command in the terminal.

Once the application is running, you can begin using the RestConfig API to retrieve values for your datacenter.

Note: If you encounter any issues during installation, make sure that you have all the prerequisites listed in the 
previous section installed and set up correctly.

## REST Endpoints
HTTP Methods

The API supports the following HTTP methods:
### POST

Use the POST method to create a new configuration resource on the server.

Example:

    curl -X POST http://IP:PORT/configs -d '{    "name": "datacenter-5",
    "metadata": {
        "monitoring": {
            "enabled": "false"
            },
            "limits": {
                "cpu": {
                    "enabled": "true",
                    "value": "true"
                        }
                    }
                }
            }'
### GET
Use the GET method to retrieve a list of all configuration resources on the server.

### GET ID
Use the `GET` method to retrieve a list of all configuration resources on the server.

    curl http://<IP:PORT>/configs

You can also use the GET method to retrieve a specific configuration resource with the specified name by using the /config/{name} endpoint.

Example:
    
    curl http://<IP:PORT>/config/datacenter-2

### PUT

Use the PUT method to update an existing configuration resource with the specified name.

Example:
    
    curl -X PUT http://<IP:PORT>/config/datacenter-3 -d '{"value": "200m"}'

### DELETE

Use the `DELETE` method to delete a configuration resource with the specified name.

Example:

    curl -X DELETE http://<IP:PORT>/config/datacenter-3

### Search Endpoint

Use the /search endpoint to search for resources with specific metadata. The metadata.key=value query parameter specifies that the search should only return resources that have a metadata key-value pair with the specified key and value.

Example:

    curl http://<IP:PORT>/configs/search?metadata.monitoring.enabled=false

