package com.okello.karest.controller;

import com.okello.karest.dto.Karest;
import com.okello.karest.service.KarestService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest()
@WebAppConfiguration
class KarestControllerTest {
    String jsonValues = "[{\"name\":\"datacenter-4\",\"metadata\":{\"monitoring\":{\"enabled\":\"false\"},\"limits\":{\"cpu\":{\"enabled\":\"false\",\"value\":\"200m\"}}}},{\"name\":\"datacenter-3\",\"metadata\":{\"monitoring\":{\"enabled\":\"false\"},\"limits\":{\"cpu\":{\"enabled\":\"false\",\"value\":\"200m\"}}}}]";
    @Autowired
    private MockMvc mockMvc;
    @Test
    public void getKarest() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders
                .get("/configs")
                .accept(MediaType.APPLICATION_JSON);
       MvcResult result = mockMvc.perform(request)
               .andExpect(status().isOk())
               .andExpect(content().string(jsonValues))
               .andReturn();

    }


}