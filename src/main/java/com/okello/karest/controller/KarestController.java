package com.okello.karest.controller;

import com.okello.karest.dto.*;
import com.okello.karest.errors.AppExceptionHandler;
import com.okello.karest.mapper.KarestMapper;
import com.okello.karest.middleware.QueueProducer;
import com.okello.karest.middleware.RabbitMQJsonProducer;
import com.okello.karest.model.KarestModel;
import com.okello.karest.service.KarestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@Slf4j

public class KarestController {
    @Autowired
    private KarestService karestService;
    @Autowired
    private QueueProducer producer;

    private RabbitMQJsonProducer jsonProducer;

   /* public KarestController(QueueProducer producer) {
        this.producer = producer;
    }

    //The message for the application to get a message from
    @GetMapping(value = "/sender")
    public ResponseEntity<String> producer(@RequestParam("message")String message){
        producer.sendMessage(message);
        return ResponseEntity.ok("Message sent successfully");
    }
    //The message for the application to send a json message to
    @PostMapping(value = "/sender")
    public ResponseEntity<String> sendJsonMessage(@RequestBody KarestMapper karestMapper){
        this.jsonProducer = jsonProducer;
        System.out.printf(String.valueOf(karestMapper.toDto(new KarestModel())));
        return ResponseEntity.ok(String.valueOf(karestMapper.toDto(new KarestModel())));
    }*/

    @GetMapping(value = "/configs")
    public List<Karest> getKarest(){
        return karestService.getConfigs();
    }

    @GetMapping(value = "/configs/{name}")
    public Karest getKarestId(@PathVariable("name") String name){
       log.info("Getting the config for {}",name);
       return getConfigOrThrow(name);
    }

    @PostMapping(value = "/configs")
    public Karest postKarest(@RequestBody Karest karest){
        return karestService.addKarest(karest);
    }

    @PutMapping(value = "/configs/{name}")
    public Karest putKarestId(@RequestBody Karest karest,@PathVariable String name){
        return karestService.updateKarest(name,karest);
    }

    @DeleteMapping(value = "/configs/{name}")
    public int removeKarest(@PathVariable String name){
        return karestService.removeConfig(name);
    }

    private Karest getConfigOrThrow(String name) throws AppExceptionHandler {
        try {
            return karestService.getConfig(name);
        } catch (Exception e) {
            throw new AppExceptionHandler("The value does not exist");
        }
    }
       for(Karest k : restndogo){
           if(k.getName().equalsIgnoreCase(name)){
               karest = k;
               restndogo.remove(k);
               log.info(restndogo.toString());
               break;
           }
       }
       return ResponseEntity.ok(null);
   }
}
