package com.okello.karest.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "karest")
public class KarestModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @Column(nullable = false,unique = true)
    private String name;


    @OneToOne(cascade = CascadeType.REMOVE,orphanRemoval = true)
    @JoinColumn(name = "metadata_id",referencedColumnName = "id")
    private MetadataModel metadata;
}
