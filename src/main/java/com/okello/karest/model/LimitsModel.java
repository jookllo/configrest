package com.okello.karest.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="limits")
public class LimitsModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(cascade = CascadeType.REMOVE,orphanRemoval = true)
    @JoinColumn(name = "cpuId")
    private CpuModel cpu;
}
