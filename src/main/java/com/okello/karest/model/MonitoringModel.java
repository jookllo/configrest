package com.okello.karest.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "monitoring")
@Data
public class MonitoringModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String enabled;
}
