package com.okello.karest.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "metadata")
@Data
public class MetadataModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne(cascade = CascadeType.REMOVE,orphanRemoval = true)
    @JoinColumn(name = "monitoring_id",referencedColumnName = "id")
    private MonitoringModel monitoring;

    @OneToOne(cascade = CascadeType.REMOVE,orphanRemoval = true)
    @JoinColumn(name = "limits_id", referencedColumnName = "id")
    private LimitsModel limits;

}
