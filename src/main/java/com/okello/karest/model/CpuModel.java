package com.okello.karest.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="cpu")
public class CpuModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String enabled;
    private String value;
}
