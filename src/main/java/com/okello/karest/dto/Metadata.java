
package com.okello.karest.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "monitoring",
    "limits"
})
public class Metadata {

    @JsonProperty("monitoring")
    private Monitoring monitoring;
    @JsonProperty("limits")
    private Limits limits;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();



    @JsonProperty("monitoring")
    public Monitoring getMonitoring() {
        return monitoring;
    }

    @JsonProperty("monitoring")
    public void setMonitoring(Monitoring monitoring) {
        this.monitoring = monitoring;
    }

    @JsonProperty("limits")
    public Limits getLimits() {
        return limits;
    }

    @JsonProperty("limits")
    public void setLimits(Limits limits) {
        this.limits = limits;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

  /*  public Metadata(Monitoring monitoring, Limits limits, Map<String, Object> additionalProperties) {
        this.monitoring = monitoring;
        this.limits = limits;
        this.additionalProperties = additionalProperties;
    }*/

}
