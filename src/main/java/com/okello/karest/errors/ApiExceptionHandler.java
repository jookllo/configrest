package com.okello.karest.errors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;
@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<Object> handleApiRequestException(AppExceptionHandler ex){
        HttpStatus badRequest = HttpStatus.INTERNAL_SERVER_ERROR;
        AppException z = new AppException(
                ex.getMessage(),
                badRequest,
                ZonedDateTime.now(ZoneId.of("Z")));
        return new ResponseEntity<>(z, badRequest);
    }
}
