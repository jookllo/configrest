package com.okello.karest.errors;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

public class AppExceptionHandler extends RuntimeException{
    public AppExceptionHandler(String message) {
        super(message);
    }

    public AppExceptionHandler(String message, Throwable cause) {
        super(message, cause);
    }
}
