package com.okello.karest.mapper;

import com.okello.karest.dto.*;
import com.okello.karest.model.KarestModel;
import com.okello.karest.repository.KarestRepository;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

public class KarestMapper {
    public Karest toDto(KarestModel karestModel){
        Karest karest = new Karest();

        karest.setName(karest.getName());


        Cpu cpu = new Cpu();
        cpu.setEnabled(karestModel.getMetadata().getLimits().getCpu().getEnabled());
        cpu.setValue(karestModel.getMetadata().getLimits().getCpu().getValue());


        Limits limits= new Limits();
        limits.setCpu(cpu);


        Monitoring monitoring = new Monitoring();
        monitoring.setEnabled(karestModel.getMetadata().getMonitoring().getEnabled());


        Metadata metadata = new Metadata();
        metadata.setMonitoring(monitoring);
        metadata.setLimits(limits);


        karest.setMetadata(metadata);
        return karest;
    }
    public KarestModel toModel(Karest karest){
        ModelMapper mapper = new ModelMapper();

        KarestModel karestModel = mapper.map(karest, KarestModel.class);
        return karestModel;
    }
    public List<Karest> toDtoList(List<KarestModel>karest){
        ModelMapper mapper = new ModelMapper();

        List<Karest> karestList = new ArrayList<>();
        for (KarestModel karestModel:karest) {
            karestList.add(mapper.map(karestModel, Karest.class));
        }
        return karestList;
    }
}