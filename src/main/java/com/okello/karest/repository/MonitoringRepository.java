package com.okello.karest.repository;

import com.okello.karest.model.MonitoringModel;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MonitoringRepository extends JpaRepository<MonitoringModel,Integer> {
    @Override
    List<MonitoringModel> findAll();
    MonitoringModel findByEnabled(String enabled);
    MonitoringModel findAllByEnabled(String enabled);
    MonitoringModel findById(int monitoring_id);

    @Transactional
    @Modifying
    @Query(value = "insert into monitoring(enabled) values(:enabled)",nativeQuery = true)
    int insertMonitoring(@Param("enabled")String enabled);

    @Transactional
    @Modifying
    @Query(value = "update monitoring set enabled=:enabled where id=:monitoring_id",nativeQuery = true)
    int updateMonitoring(@Param("monitoring_id")int monitoring_id,@Param("enabled")String enabled);

}
