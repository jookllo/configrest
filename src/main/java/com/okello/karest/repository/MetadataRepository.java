package com.okello.karest.repository;

import com.okello.karest.model.MetadataModel;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MetadataRepository extends JpaRepository<MetadataModel,Integer> {
    @Override
    List<MetadataModel> findAll();
    MetadataModel findAllById(int id);
    MetadataModel findById(int id);

    @Transactional
    @Modifying
    @Query(value = "insert into metadata(limits_id,monitoring_id) values(:limits_id,:monitoring_id)",nativeQuery = true)
    int insertMetadata(@Param("limits_id")int limits_id,@Param("monitoring_id")int monitoring_id);

    @Transactional
    @Modifying
    @Query(value = "update metadata set monitoring_id=:monitoring_id,limits_id=:limits_id where id=:metadata_id",nativeQuery = true)
    int updateMetadata(@Param("metadata_id")int metadata_id,@Param("monitoring_id")int monitoring_id,@Param("limits_id")int limits_id);
}
