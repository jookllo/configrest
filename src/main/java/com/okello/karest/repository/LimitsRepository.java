package com.okello.karest.repository;

import com.okello.karest.model.LimitsModel;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LimitsRepository extends JpaRepository<LimitsModel,Integer> {
    @Override
    List<LimitsModel> findAll();
    LimitsModel findByCpuId(int cpuId);
    LimitsModel findAllByCpuId(int cpuId);
    LimitsModel findAllById(int limits_Id);

    @Transactional
    @Modifying
    @Query(value = "insert into limits(cpu_id) value (:cpuId)",nativeQuery = true)
    int insertLimits(@Param("cpuId")int cpuId);

    @Transactional
    @Modifying
    @Query(value = "update limits set cpu_id=:cpuId where id=:limits_id",nativeQuery = true)
    int updateLimits(@Param("limits_id")int limits_id,@Param("cpuId")int cpuId);
}
