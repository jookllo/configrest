package com.okello.karest.repository;

import com.okello.karest.model.KarestModel;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface KarestRepository extends JpaRepository<KarestModel, Integer> {
    @Override
    List<KarestModel> findAll();
    KarestModel findAllByName(String name);
    KarestModel findByName(String name);

    @Transactional
    int deleteByName(String name);

    @Transactional
    @Modifying
    @Query(value = "Insert into karest(name,metadata_id)values(:name,:metadata_id)", nativeQuery = true)
    int insertConfig(@Param("name")String name, @Param("metadata_id")long metadata_id);

    @Transactional
    @Modifying
    @Query(value = "update karest set name=:name,metadata_id=:metadata_id where id = :karest_id", nativeQuery = true)
    int updateConfig(@Param("karest_id") long karest_id, @Param("name")String name, @Param("metadata_id") long metadata_id);
}
