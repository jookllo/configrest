package com.okello.karest.repository;

import com.okello.karest.model.CpuModel;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CpuRepository extends JpaRepository<CpuModel,Integer> {
    List<CpuModel> findAllBy();
    CpuModel findByValue(String value);
    CpuModel findAllByValue(String value);
    CpuModel deleteById(int cpuId);

    @Transactional
    @Modifying
    @Query(value = "insert into cpu(enabled,value) VALUES(:enabled,:value)", nativeQuery = true)
    int insertCpu(@Param("value")String value, @Param("enabled")String enabled);

    @Transactional
    @Modifying
    @Query(value = "update cpu set value=:value,enabled=:enabled where id=:cpuId",nativeQuery = true)
    int updateCpu(@Param("cpuId")int cpuId,@Param("value")String value,@Param("enabled")String enabled);
}
