package com.okello.karest.middleware;

import com.okello.karest.dto.Karest;
import com.okello.karest.mapper.KarestMapper;
import com.okello.karest.model.KarestModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQJsonProducer {

    @Value("${karest.exchange.name}")
    private String exchange;
    @Value("${karest.routing.json.key}")
    private String routingJsonKey;

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQJsonProducer.class);
    private RabbitTemplate rabbitTemplate;

    public RabbitMQJsonProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendJsonMessage(KarestMapper karestMapper){
        LOGGER.info(String.format("Json message sent -> %s",karestMapper.toString()));
        rabbitTemplate.convertAndSend(exchange,routingJsonKey,karestMapper);
    }
}
