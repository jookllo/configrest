package com.okello.karest.middleware;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class QueueConsumer {
    private CountDownLatch latch = new CountDownLatch(1);
     public void receiveMessage(String message){
         System.out.println("Received <"+ message+">");
         latch.countDown();
     }
     public CountDownLatch getLatch(){
         return latch;
     }
}
