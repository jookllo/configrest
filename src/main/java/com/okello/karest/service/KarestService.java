package com.okello.karest.service;

import com.okello.karest.dto.*;
import com.okello.karest.mapper.KarestMapper;
import com.okello.karest.model.*;
import com.okello.karest.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;

@Service
@Slf4j
public class KarestService {
   @Autowired
   private CpuRepository cpuRepository;
   @Autowired
   private AmqpTemplate rabbitTemplate;
   @Autowired
   private Queue queue;
   @Autowired
   private KarestRepository karestRepository;
   @Autowired
   private LimitsRepository limitsRepository;
   @Autowired
   private MonitoringRepository monitoringRepository;
   @Autowired
   private MetadataRepository metadataRepository;

   private static Logger logger = LogManager.getLogManager().getLogger(KarestService.class.toString());
    KarestMapper mapper = new KarestMapper();

    ModelMapper mapper1 = new ModelMapper();
    public List<Karest> getConfigs(){
       List<KarestModel> karests = karestRepository.findAll();
       return mapper.toDtoList(karests);
   }

   public Karest getConfig(String name){
       KarestModel karest = karestRepository.findByName(name);
       log.info("Karest Model {}",karest);
       KarestMapper mapper = new KarestMapper();
       return mapper.toDto(karest);

   }
   public void sendConfig(Karest karest){
        rabbitTemplate.convertAndSend(queue.getName(),karest);
        logger.info("Sending Message to Queue: "+karest.toString());
   }
   public Karest addKarest(Karest karest){
       ModelMapper karestMapper = new ModelMapper();

       KarestModel karestModel = karestMapper.map(karest, KarestModel.class);

       int cpuId = cpuRepository.insertCpu(karest.getMetadata().getLimits().getCpu().getEnabled(),
               karest.getMetadata().getLimits().getCpu().getValue());

       int limits_id = limitsRepository.insertLimits(cpuId);

       int monitoring_id = monitoringRepository.insertMonitoring(karest.getMetadata().getMonitoring().getEnabled());

       int metadata_id = metadataRepository.insertMetadata(monitoring_id,limits_id);

       karestRepository.insertConfig(karestModel.getName(),metadata_id);
       rabbitTemplate.convertAndSend(queue.getName(),karest);
       logger.info("Sending Message to Queue: "+karest.toString());

       return mapper.toDto(karestModel);
   }
   private Karest createKarest(String name,String mointoringEnabled,String cpuEnabled,String cpuValue){
       CpuModel cpu = new CpuModel();
       cpu.setEnabled(cpuEnabled);
       cpu.setValue(cpuValue);

       LimitsModel limitsModel = new LimitsModel();
       limitsModel.setCpu(cpu);

       MonitoringModel monitoringModel = new MonitoringModel();
       monitoringModel.setEnabled(mointoringEnabled);

       MetadataModel metadataModel = new MetadataModel();
       metadataModel.setMonitoring(monitoringModel);
       metadataModel.setLimits(limitsModel);

       KarestModel karestModel = new KarestModel();
       karestModel.setName(name);
       karestModel.setMetadata(metadataModel);

       return mapper1.map(karestModel,Karest.class);

   }

   public Karest updateKarest(String name,Karest karest){
        KarestModel karestModel = mapper.toModel(karest);
        KarestModel karestModel1 = karestRepository.findByName(name);
        //naming convention should fit in to what is to be achieved

        cpuRepository.updateCpu(karestModel1.getMetadata().getLimits().getCpu().getId(),
                karest.getMetadata().getLimits().getCpu().getValue(),
                karest.getMetadata().getLimits().getCpu().getEnabled());

        limitsRepository.updateLimits(karestModel1.getMetadata().getLimits().getId(),
                karestModel1.getMetadata().getLimits().getCpu().getId());

        monitoringRepository.updateMonitoring(karestModel1.getMetadata().getLimits().getId(),
                karest.getMetadata().getLimits().getCpu().getEnabled());

        metadataRepository.updateMetadata(karestModel1.getMetadata().getId(),
                karestModel1.getMetadata().getMonitoring().getId(),
                karestModel1.getMetadata().getLimits().getId());

        karestRepository.updateConfig(karestModel1.getId(),karest.getName(),karestModel1.getMetadata().getId());

        return karest;
        //make a specific response.
   }

   public int removeConfig(String name){
        return karestRepository.deleteByName(name);
   }
}

